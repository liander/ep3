class AddProfessorToTurma < ActiveRecord::Migration[6.0]
  def change
    add_column :turmas, :references, :professor
  end
end
