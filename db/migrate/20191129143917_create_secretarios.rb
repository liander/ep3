class CreateSecretarios < ActiveRecord::Migration[6.0]
  def change
    create_table :secretarios do |t|
      t.string :nome
      t.string :identificacao

      t.timestamps
    end
  end
end
