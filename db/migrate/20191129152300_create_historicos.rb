class CreateHistoricos < ActiveRecord::Migration[6.0]
  def change
    create_table :historicos do |t|
      t.references :aluno, null: false, foreign_key: true
      t.integer :turma_id
      t.string :mencao

      t.timestamps
    end
  end
end
