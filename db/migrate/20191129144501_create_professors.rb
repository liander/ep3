class CreateProfessors < ActiveRecord::Migration[6.0]
  def change
    create_table :professors do |t|
      t.string :nome
      t.string :matricula
      t.string :formacao

      t.timestamps
    end
  end
end
