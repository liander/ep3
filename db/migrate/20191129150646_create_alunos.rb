class CreateAlunos < ActiveRecord::Migration[6.0]
  def change
    create_table :alunos do |t|
      t.string :nome
      t.string :matricula
      t.references :turma, null: false, foreign_key: false

      t.timestamps
    end
  end
end
