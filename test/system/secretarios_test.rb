require "application_system_test_case"

class SecretariosTest < ApplicationSystemTestCase
  setup do
    @secretario = secretarios(:one)
  end

  test "visiting the index" do
    visit secretarios_url
    assert_selector "h1", text: "Secretarios"
  end

  test "creating a Secretario" do
    visit secretarios_url
    click_on "New Secretario"

    fill_in "Identificacao", with: @secretario.identificacao
    fill_in "Nome", with: @secretario.nome
    click_on "Create Secretario"

    assert_text "Secretario was successfully created"
    click_on "Back"
  end

  test "updating a Secretario" do
    visit secretarios_url
    click_on "Edit", match: :first

    fill_in "Identificacao", with: @secretario.identificacao
    fill_in "Nome", with: @secretario.nome
    click_on "Update Secretario"

    assert_text "Secretario was successfully updated"
    click_on "Back"
  end

  test "destroying a Secretario" do
    visit secretarios_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Secretario was successfully destroyed"
  end
end
