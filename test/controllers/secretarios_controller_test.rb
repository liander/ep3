require 'test_helper'

class SecretariosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @secretario = secretarios(:one)
  end

  test "should get index" do
    get secretarios_url
    assert_response :success
  end

  test "should get new" do
    get new_secretario_url
    assert_response :success
  end

  test "should create secretario" do
    assert_difference('Secretario.count') do
      post secretarios_url, params: { secretario: { identificacao: @secretario.identificacao, nome: @secretario.nome } }
    end

    assert_redirected_to secretario_url(Secretario.last)
  end

  test "should show secretario" do
    get secretario_url(@secretario)
    assert_response :success
  end

  test "should get edit" do
    get edit_secretario_url(@secretario)
    assert_response :success
  end

  test "should update secretario" do
    patch secretario_url(@secretario), params: { secretario: { identificacao: @secretario.identificacao, nome: @secretario.nome } }
    assert_redirected_to secretario_url(@secretario)
  end

  test "should destroy secretario" do
    assert_difference('Secretario.count', -1) do
      delete secretario_url(@secretario)
    end

    assert_redirected_to secretarios_url
  end
end
