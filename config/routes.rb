Rails.application.routes.draw do
  devise_for :profs
  devise_for :admins
  root 'home#inicio'
  resources :historicos
  resources :alunos
  resources :turmas
  resources :professors
  resources :secretarios
  devise_for :user
  get 'secretario', to: 'secretarios#opcoes_secretario'
  get 'aluno', to: 'alunos#opcoes_aluno'
  get 'professor', to: 'professors#opcoes_professor'
  get 'secretario1', to: 'secretarios#index'
  get 'aluno1', to: 'alunos#index'
  get 'professor1', to: 'professors#index'
  get 'aluno_historico', to: 'historicos#index'
  get 'turma', to: 'turmas#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
