# README

## Unb idiomas por Liander e Carlos

**Tentamos implementar esta ideia inicialmete neste repositório, mas infelizmente encontramos problemas que não conseguimos lidar. Como solução decidimos começãr do zero com outro repositório, o ep4, mas não conseguimos concluí-lo.**

**A aplicação consiste em um sistema para gerenciamento de uma escola de idiomas baseada em três usuários:**
* Aluno
* Secretário
* Professor

### Aluno
O aluno possui usuário comum que poderia ser criado apenas pelo secretário, que possui recursos admin, logo o aluno só poderia dar login no sistema e ver em que turma estava cadastrado e seu histórico. obs: não implementado completamente.

### Secretário
Possui recursos admin e pode alterar qualquer dado assim como criar logins aluno e professor, assim como criar turmas e históricos. obs: não implementado completamente.
    
### Professor
Possui habilidade de criar turmas e acessar os alunos nela presente. obs: não implementado completamente.
    