json.extract! professor, :id, :nome, :matricula, :formacao, :created_at, :updated_at
json.url professor_url(professor, format: :json)
