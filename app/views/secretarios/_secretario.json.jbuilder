json.extract! secretario, :id, :nome, :identificacao, :created_at, :updated_at
json.url secretario_url(secretario, format: :json)
