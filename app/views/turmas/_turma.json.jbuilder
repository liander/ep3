json.extract! turma, :id, :nome, :nivel, :professor_id, :created_at, :updated_at
json.url turma_url(turma, format: :json)
