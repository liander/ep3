json.extract! aluno, :id, :nome, :matricula, :turma_id, :created_at, :updated_at
json.url aluno_url(aluno, format: :json)
